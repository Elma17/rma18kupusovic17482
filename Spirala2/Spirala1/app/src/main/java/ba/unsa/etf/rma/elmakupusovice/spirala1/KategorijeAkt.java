package ba.unsa.etf.rma.elmakupusovice.spirala1;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ba.unsa.etf.rma.elmakupusovice.spirala1.fragments.ListeFragment;

public class KategorijeAkt extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kategorije_akt);

        String[] values = new String[] { "elma", "prozor" };
        final List<String> listaKat = new ArrayList<String>(Arrays.asList(values));
        String[] autori = new String[] { "nana", "dedo" };
        final List<String> listaAut = new ArrayList<String>(Arrays.asList(autori));
        final List<Knjiga> listaKnj = new ArrayList<>();

        ListeFragment lf = new ListeFragment();
        Bundle bundle = new Bundle();

        Bundle b1 = new Bundle();
        b1.putStringArrayList("listaKat", (ArrayList<String>) listaKat);
        Bundle b2 = new Bundle();
        b2.putStringArrayList("listaAut", (ArrayList<String>) listaAut);

        bundle.putBundle("listaKat", b1);
        bundle.putBundle("listaAut", b2);
        lf.setArguments(bundle);

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.your_placeholder, lf);
        ft.commit();



        /*

        final Button dPretraga = (Button)findViewById(R.id.dPretraga);
        final Button dDodajKategoriju = (Button)findViewById(R.id.dDodajKategoriju);
        final Button dDodajKnjigu = (Button)findViewById(R.id.dDodajKnjigu);
        final ListView listaKategorija = (ListView)findViewById(R.id.listaKategorija);
        final TextView tekstPretraga = (TextView)findViewById(R.id.tekstPretraga);

        final Button dKategorija = (Button)findViewById(R.id.dKategorije);

        final ArrayList<Knjiga> listaKnjiga = new ArrayList<>();
        //Knjiga k1 = new Knjiga();

        String[] values = new String[] { "elma", "prozor" };

        final List<String> listaKat = new ArrayList<String>(Arrays.asList(values));

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, listaKat);
        //listaKategorija.setAdapter(adapter);

        dDodajKategoriju.setEnabled(false);

        dPretraga.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public  void onClick(View v)
            {
                adapter.getFilter().filter(tekstPretraga.getText().toString());
                adapter.notifyDataSetChanged();

                if (adapter.getCount() == 0)
                {
                    dDodajKategoriju.setEnabled(true);
                }
                else
                {
                    dDodajKategoriju.setEnabled(false);
                }

            }
        });

        dDodajKategoriju.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (tekstPretraga.getText().toString() != "")
                    adapter.add(tekstPretraga.getText().toString());
                tekstPretraga.setText("");
            }
        });
        dDodajKnjigu.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                ArrayList<String> items = new ArrayList<String>();
                for(int i = 0; i < adapter.getCount(); i++){
                    items.add(adapter.getItem(i));
                }

                Fragment fDodavanjeKnjige = new DodavanjeKnjigeFragment();
                fragmentTransaction.replace(R.id.fragment_container , fDodavanjeKnjige);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                Intent intent = new Intent(KategorijeAkt.this, DodavanjeKnjigeFragment.class);
                intent.putExtra("kategorije", (ArrayList<String>)items);
                intent.putExtra("listaKnjiga", (ArrayList<Knjiga>)listaKnjiga);
                startActivity(intent);
            }
        });*/

/*
        listaKategorija.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public  void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Intent myIntent = new Intent(KategorijeAkt.this, ListaKnjigaAkt.class);
                myIntent.putExtra("listaKnjiga", (ArrayList<Knjiga>)listaKnjiga);
                myIntent.putExtra("kategorija", listaKategorija.getSelectedItem().toString());
                startActivity(myIntent);
            }
        });

        dKategorija.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listaKategorija.setAdapter(adapter);
            }
        });*/

    }


}
