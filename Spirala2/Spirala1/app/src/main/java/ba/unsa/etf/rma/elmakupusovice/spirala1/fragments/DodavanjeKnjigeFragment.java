package ba.unsa.etf.rma.elmakupusovice.spirala1.fragments;

import android.Manifest;
import android.app.Fragment;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;

import ba.unsa.etf.rma.elmakupusovice.spirala1.Knjiga;
import ba.unsa.etf.rma.elmakupusovice.spirala1.R;

import static android.app.Activity.RESULT_OK;


public class DodavanjeKnjigeFragment extends Fragment{
    ImageView mImageview;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        final View view = inflater.inflate(R.layout.fragment_dodavanje_knjige_akt, container, false);
        mImageview = (ImageView) view.findViewById(R.id.naslovnaStr);

        view.findViewById(R.id.dNadjiSliku).setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                if(ActivityCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                {
                    requestPermissions(
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            2000);
                }
                else {
                    startGallery();
                }
            }
        });
        return view;
    }

    private void startGallery() {
        Intent cameraIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        cameraIntent.setType("image/*");
        if (cameraIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(cameraIntent, 1000);
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed
        if(resultCode == RESULT_OK) {
            if(requestCode == 1000){
                Uri returnUri = data.getData();
                Bitmap bitmapImage = null;
                try {
                    bitmapImage = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), returnUri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                mImageview.setImageBitmap(bitmapImage);
            }
        }}
    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState)
    {
        Button dPonisti = (Button)view.findViewById(R.id.dPonisti);
        Button dNadjiSliku = (Button)view.findViewById(R.id.dNadjiSliku);
        Button dUpisiKnjigu = (Button)view.findViewById(R.id.dUpisiKnjigu);
        final Spinner sKategorijaKnjige = (Spinner)view.findViewById(R.id.sKategorijaKnjige);

        ArrayList<String> list = (ArrayList<String>) getArguments().getBundle("listaKat").getStringArrayList("listaKat");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sKategorijaKnjige.setAdapter(adapter);




        dPonisti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });

        dUpisiKnjigu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final TextView nazivKnjige = (TextView)view.findViewById(R.id.nazivKnjige);
                final TextView imeAutora = (TextView)view.findViewById(R.id.imeAutora);
                final ImageView naslovnaStr = (ImageView)view.findViewById(R.id.naslovnaStr);
                Knjiga k = new Knjiga(nazivKnjige.getText().toString(), imeAutora.getText().toString(), sKategorijaKnjige.getSelectedItem().toString(),((BitmapDrawable)naslovnaStr.getDrawable()).getBitmap());

                (getArguments().getBundle("listaKnj").getParcelableArrayList("listaKnj")).add(k);
                //(getArguments().getBundle("listaKat").getStringArrayList("listaKat")).add(k.imeAutora);
            }
        });

    }




}
