package ba.unsa.etf.rma.elmakupusovice.spirala1;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class ListaKnjigaAkt extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_knjiga_akt);

        Button Povratak = (Button)findViewById(R.id.dPovratak);

        Povratak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ListaKnjigaAkt.super.onBackPressed();
            }
        });
    }
}
