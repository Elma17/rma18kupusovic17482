package ba.unsa.etf.rma.elmakupusovice.spirala1.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ba.unsa.etf.rma.elmakupusovice.spirala1.Knjiga;
import ba.unsa.etf.rma.elmakupusovice.spirala1.R;

public class ListeFragment extends Fragment{

    List<Knjiga> listaKnj;
    ArrayList<String> listaKat;
    ArrayList<String> listaAut;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        listaKnj = new ArrayList<>();
        listaAut = new ArrayList<>();
        listaKat = getArguments().getBundle("listaKat").getStringArrayList("listaKat");

        return  inflater.inflate(R.layout.fragment_activity_kategorije_akt, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        final Button dPretraga = (Button)view.findViewById(R.id.dPretraga);
        final Button dDodajKategoriju = (Button)view.findViewById(R.id.dDodajKategoriju);
        final Button dDodajKnjigu = (Button)view.findViewById(R.id.dDodajKnjigu);
        final ListView listaKategorija = (ListView)view.findViewById(R.id.listaKategorija);
        final TextView tekstPretraga = (TextView)view.findViewById(R.id.tekstPretraga);

        final Button dKategorija = (Button)view.findViewById(R.id.dKategorije);
        final Button dAutori = (Button)view.findViewById(R.id.dAutori);

        dDodajKategoriju.setEnabled(false);

        String[] values = new String[] { };
        final List<String> l1 = new ArrayList<String>(Arrays.asList(values));
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, l1);
        listaKategorija.setAdapter(adapter);


        dPretraga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.getFilter().filter(tekstPretraga.getText().toString());
                adapter.notifyDataSetChanged();

                if (adapter.getCount() == 0)
                {
                    dDodajKategoriju.setEnabled(true);
                }
                else
                {
                    dDodajKategoriju.setEnabled(false);
                }
            }
        });
        dDodajKategoriju.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                {
                    if (tekstPretraga.getText().toString() != "")
                    {
                        listaKat.add(tekstPretraga.getText().toString());
                        adapter.add(tekstPretraga.getText().toString());
                        adapter.notifyDataSetChanged();
                        listaKategorija.invalidateViews();
                        tekstPretraga.setText("");
                    }
                }
            }
        });

        dKategorija.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dPretraga.setVisibility(View.VISIBLE);
                dDodajKategoriju.setVisibility(View.VISIBLE);
                tekstPretraga.setVisibility(View.VISIBLE);

                adapter.clear();
                adapter.addAll(listaKat);
                adapter.notifyDataSetChanged();
                listaKategorija.invalidateViews();
            }
        });
        dDodajKnjigu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle = new Bundle();
                Bundle b1 = new Bundle();
                b1.putParcelableArrayList("listaKnj", (ArrayList<? extends Parcelable>) listaKnj);
                Bundle b2 = new Bundle();
                b2.putStringArrayList("listaKat", listaKat);

                bundle.putBundle("listaKnj", b1);
                bundle.putBundle("listaKat", b2);

                /*String aut = "d";
                String kat = " ";
                String naz = "n";
                Bitmap b = BitmapFactory.decodeResource(getResources(),R.drawable.book1);

                bundle.putString("imeAutora", aut);
                bundle.putString("kat",kat);
                bundle.putString("naz", naz);
                bundle.putParcelable("b", b);*/

                DodavanjeKnjigeFragment f = new DodavanjeKnjigeFragment();
                f.setArguments(bundle);
                getFragmentManager().beginTransaction().replace(R.id.your_placeholder, f).addToBackStack(null).commit();
            }
        });
        dAutori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dPretraga.setVisibility(View.GONE);
                dDodajKategoriju.setVisibility(View.GONE);
                tekstPretraga.setVisibility(View.GONE);

                for (Knjiga k1: listaKnj) {
                    listaAut.add(k1.imeAutora);
                }
                //listaAut.add(String.valueOf(listaKnj.size()));
                //ArrayList<String> arraylist = getArguments().getBundle("listaAut").getStringArrayList("listaAut");
                adapter.clear();
                adapter.addAll(listaAut);
                adapter.notifyDataSetChanged();
                listaKategorija.invalidateViews();
            }
        });


    }

}
