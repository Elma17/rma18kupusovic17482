package ba.unsa.etf.rma.elmakupusovice.spirala1;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Elma Kupusovi on 27.3.2018.
 */

public class Knjiga implements Parcelable{

    public String nazivKnjige;
    public String imeAutora;
    public String kategorija;
    public Bitmap naslovnaStr;

    public Knjiga(String nazivKnjige, String imeAutora, String kategorija, Bitmap naslovnaStr) {
        this.nazivKnjige = nazivKnjige;
        this.imeAutora = imeAutora;
        this.kategorija = kategorija;
        this.naslovnaStr = naslovnaStr;
    }


    protected Knjiga(Parcel in) {
        nazivKnjige = in.readString();
        imeAutora = in.readString();
        kategorija = in.readString();
        naslovnaStr = in.readParcelable(Bitmap.class.getClassLoader());
    }

    public static final Creator<Knjiga> CREATOR = new Creator<Knjiga>() {
        @Override
        public Knjiga createFromParcel(Parcel in) {
            return new Knjiga(in);
        }

        @Override
        public Knjiga[] newArray(int size) {
            return new Knjiga[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nazivKnjige);
        dest.writeString(imeAutora);
        dest.writeString(kategorija);
        dest.writeParcelable(naslovnaStr, flags);
    }
}
