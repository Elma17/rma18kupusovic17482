package ba.unsa.etf.rma.elmakupusovice.spirala1;

import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

public class DodavanjeKnjigeAkt extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dodavanje_knjige_akt);

        final Spinner sKategorijaKnjige = (Spinner)findViewById(R.id.sKategorijaKnjige);
        Button dPonisti = (Button)findViewById(R.id.dPonisti);
        Button dUpisiKnjigu = (Button)findViewById(R.id.dUpisiKnjigu);

        final TextView nazivKnjige = (TextView)findViewById(R.id.nazivKnjige);
        final TextView imeAutora = (TextView)findViewById(R.id.imeAutora);
        final ImageView naslovnaStr = (ImageView)findViewById(R.id.naslovnaStr);

        ArrayList<String> list = (ArrayList<String>) getIntent().getSerializableExtra("kategorije");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sKategorijaKnjige.setAdapter(adapter);

        dPonisti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DodavanjeKnjigeAkt.super.onBackPressed();
            }
        });

        final ArrayList<Knjiga> niz = (ArrayList<Knjiga>)getIntent().getSerializableExtra("listaKnjga");
        
        dUpisiKnjigu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Knjiga k = new Knjiga(nazivKnjige.getText().toString(), imeAutora.getText().toString(), sKategorijaKnjige.getSelectedItem().toString(),((BitmapDrawable)naslovnaStr.getDrawable()).getBitmap());
                niz.add(k);
            }
        });

    }
}
